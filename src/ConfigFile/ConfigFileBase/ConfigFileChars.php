<?php

namespace Phr\FileManager\ConfigFile\ConfigFileBase;


interface ConfigFileChars 
{
    public const CNFG = '=';

    public const NEWLINE = "\n";
}