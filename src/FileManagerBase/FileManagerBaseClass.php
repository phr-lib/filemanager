<?php

namespace Phr\FileManager\FileManagerBase;

abstract class FileManagerBaseClass
{   
    protected $content;
    /**
     * @access protected
     * @var static
     * @var fullFileName
     * @var string
     * Contains full file name and file path 
     * 
     */
    protected static string $fullFileName;

    /**
     * @var file
     * @var string
     * Name of the file
     */
    protected static string $file;

    /**
     * @var filePath
     * @var string
     */
    protected static string $filePath;

    /**
     * @var fileExt
     * @var string
     * Provides file extention
     */
    protected static string $fileExt;

    // CONSTRUCTOR ***
    public function __construct( string $_full_file_name ){

        self::$fullFileName = $_full_file_name;

        self::brakeFilePath();
    }
    /**
     * @access private
     */
    private static function brakeFilePath(): void 
    {
        $BrakeFilePath = explode( DIRECTORY_SEPARATOR, self::$fullFileName );

        self::$file = array_pop( $BrakeFilePath );

        self::$filePath = implode( DIRECTORY_SEPARATOR, $BrakeFilePath );

        self::$fileExt = pathinfo(self::$file, PATHINFO_EXTENSION);
    }
}