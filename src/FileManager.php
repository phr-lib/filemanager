<?php

namespace Phr\FileManager;

use Phr\FileManager\Subvention;
use Phr\FileManager\ConfigFile\Subvention\ConfigLine;
use Phr\FileManager\Generator\CreateFile;


final class FileManager extends FileManagerBase\FileManagerBaseClass
{   
    public function fileExtention(): string 
    {
        return self::$fileExt;
    }
    public function fileName(): string 
    {
        return self::$file;
    }
    /**
     * @method content
     * It add content to file 
     */
    public function content( $_content ): void 
    {
        $this->content = $_content;
    }
    /**
     * @method addConfigContent
     * @var ConfigLine
     * Add config line to config content array
     */
    public function addConfigContent( ConfigLine $_config_line)
    {
        array_push($this->configContentArray, $_config_line);
    }
    /**
     * @method create
     * It create simple file with content
     */
    final public function create(): void 
    {   
        Subvention\DirPath::generate( self::$filePath );
        Generator\CreateFile::file( self::$fullFileName, $this->content );
    }
    /**
     * @method append
     * It create simple file with content
     */
    final public function append( mixed $_content ): void 
    {   
        Generator\CreateFile::append( self::$fullFileName, $_content );
    }
    /**
     * @method createConfig
     * Create new config file
     */
    final public function createConfig(): void 
    {
        Subvention\DirPath::generate( self::$filePath );
        $NewConfigFile = new CreateFile(self::$filePath.'cdf.config');
        $NewConfigFile->writeFile( $this->configContentArray );
    }

    /**
     * @access private
     * @var configContentArray
     * Contains config lines
     */
    private array $configContentArray = [];
}