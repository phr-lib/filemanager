<?php

namespace Phr\FileManager\ConfigFile\Subvention;

use Phr\FileManager\ConfigFile\ConfigFileBase\ConfigFileChars as CH;

class ConfigLine 
{   
    private string $key;

    private string $value;

    public function __construct( string $_key, string $_value ){

        $this->key = $_key;

        $this->value = $_value;

    }

    public function parse(): string 
    {
        return $this->key.CH::CNFG.$this->value.CH::NEWLINE;
    }
}
