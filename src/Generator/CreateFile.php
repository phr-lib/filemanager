<?php

namespace Phr\FileManager\Generator;

class CreateFile 
{   
    /**
     * @access public
     * @method file
     * @var fullFilePath
     * @var mixed content
     * @return bool true if file was created
     */
    public static function file( string $_full_file_path, mixed $_content ): bool 
    {
        return file_put_contents( $_full_file_path, $_content );
    }
    /**
     * @method append
     * @var fullFilepath
     * @var content
     * @return bool
     * Appends content to existing file
     */
    public static function append( string $_full_file_path, mixed $_content ): bool 
    {
        return file_put_contents( $_full_file_path, "\n".$_content, FILE_APPEND | LOCK_EX );
    }

    /**
     * @method writeFile
     * @see createWritenFile
     */
    public function writeFile( array $_content ): void 
    {
        $this->createWritenFile( $_content );
    }

    // CONSTRUCTOR ***
    public function __construct( string|null $_full_file_path ){

        $this->fullFilePath = $_full_file_path;
    }
    /**
     * @access private
     */
    private string $fullFilePath;

    private function createWritenFile( array $_content ): void 
    {   
        $handler = fopen( $this->fullFilePath, "w");

        foreach($_content as $configLine)
                fwrite($handler,$configLine->parse());

        fclose($handler);
       
    }
}
